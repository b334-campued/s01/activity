import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;

        Scanner userObj = new Scanner(System.in);

        System.out.println("What is your First Name:");
        firstName = userObj.nextLine();

        System.out.println("What is your Last Name:");
        lastName = userObj.nextLine();

        System.out.println("What is your First Subject Grade:");
        firstSubject = Double.parseDouble(userObj.nextLine());

        System.out.println("What is your Second Subject Grade:");
        secondSubject = Double.parseDouble(userObj.nextLine());

        System.out.println("What is your Third Subject Grade:");
        thirdSubject = Double.parseDouble(userObj.nextLine());

        double averageGrade = Math.floor((firstSubject + secondSubject + thirdSubject) / 3);

        int averageGradeInt = (int) averageGrade;

        System.out.println("Good day, John Smith.");
        System.out.println("Your grade average is: " + averageGradeInt);

    }
}